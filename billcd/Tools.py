__author__ = 'billcd'


import imghdr
import zipfile
import os
import subprocess
import sqlite3


class Tools:

    @staticmethod
    def touch(path):
        with open(path, 'a'):
            os.utime(path, None)


    @staticmethod
    def get_file_size(file, size_display = "mb"):
        bytes =  float(os.path.getsize(file))

        if size_display is "mb":
            return bytes/1024/1024

    @staticmethod
    def get_file_type(file):
        res = subprocess.check_output(["file", file])
        r = res.split(':')

        # ["png", "mov", "jpg", "mpeg", "vcf", "xml", "sqlite", "plist", "gsm", "empty", "data", "txt", "gif",
        # "aac", "pdf", "html"]:

        if "PNG image data" in r[1]:
            return "png"
        elif "ISO Media, Apple QuickTime movie" in r[1]:
            return "mov"
        elif "JPEG image data" in r[1]:
            return "jpg"
        elif "ISO Media, MPEG v4 system, 3GPP" in r[1]:
            return "mpeg"
        elif "vCard" in r[1]:
            return "vcf"
        elif "XML" in r[1]:
            return "xml"
        elif "SQLite" in r[1]:
            return "sqlite"
        elif "Apple binary property list" in r[1]:
            return "plist"
        elif "Adaptive Multi-Rate Codec (GSM telephony)" in r[1]:
            return "gsm"
        elif "ASCII" in r[1]:
            return "txt"
        elif "GIF image data" in r[1]:
            return "gif"
        elif "ISO Media, MPEG v4 system, iTunes AAC-LC" in r[1]:
            return "aac"
        elif "PDF document" in r[1]:
            return "pdf"
        elif "HTML document text" in r[1]:
            return "html"
        elif "data" in r[1]:
            return "data"
        elif "empty" in r[1]:
            return "empty"
        else:
            return r[1]

    @staticmethod
    def list_tables_in_sqlite(database):
        con = None
        try:
            con = sqlite3.connect(database)
            cur = con.cursor()
            cur.execute("SELECT * FROM sqlite_master WHERE type='table';")
            tables = cur.fetchall()
            con.close()
            return tables

        finally:
            pass


    @staticmethod
    def remove_file_extension(file):
        r = file.split(".")
        r.pop()

        name = r[0]
        r.pop(0)
        for n in r:
            name = name + "." + n

        return name

    @staticmethod
    def is_file_video(file):
        if "ISO Media, Apple QuickTime movie" in Tools.get_file_type(file):
            return True
        return False

    @staticmethod
    def is_file_image(file, types=['jpeg', 'png', 'gif']):
        file_type = imghdr.what(file)
        if file_type in types:
            return True
        else:
            return False


    @staticmethod
    def get_container_path(dir):
        el = dir.split('/')
        el.pop()

        if dir[0] is '/':
            path = ""
            el.pop(0)
        elif dir[0] is '~':
            path = "~"
            el.pop(0)
        else:
            path = "."

        for e in el:
            path = path + "/" + e

        return path

    @staticmethod
    def zip_dir(path, zip_file_name = "None"):
        if zip_file_name == "None":
            zip_file_name = path + '.zip'

        zipf = zipfile.ZipFile(zip_file_name, 'w')
        for root, dirs, files in os.walk(path):
            for file in files:
                zipf.write(os.path.join(root, file))
        zipf.close()





